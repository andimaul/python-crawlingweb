from django.shortcuts import render
from django.http import Http404
from tokopedia.models import TokpedProducts
from tokopedia.serializers import TokopediaSerializer,TokopediaCSVSerializer
from rest_framework import generics,authentication, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
import django_filters.rest_framework
from django.urls import reverse
from rest_framework import viewsets, status
from rest_framework.decorators import api_view,renderer_classes
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework_csv.parsers import CSVParser
from rest_framework_csv.renderers import CSVRenderer
from rest_framework_csv import renderers as r
from rest_pandas import PandasView

# Create your views here.

class TokopediaList(generics.ListAPIView):
	"""
	List all tokopedia.
	"""
	queryset = TokpedProducts.objects.all()
	serializer_class = TokopediaSerializer
	page_size = 1000
	# authentication_classes = [authentication.TokenAuthentication]
	# permission_classes = [permissions.IsAuthenticated]

	# def get(self, request, format=None):
	# 	"""
	# 	Return a list of all users.
	# 	"""
	# 	tokped = TokpedProducts.objects.all()
	# 	return tokped

class TokopediaDetail(generics.RetrieveUpdateDestroyAPIView):
	"""
	Retrieve, update or delete a snippet instance.
	"""
	queryset = TokpedProducts.objects.all()
	serializer_class = TokopediaSerializer


class TokopediaFilterScheduleID(generics.ListAPIView):
	serializer_class = TokopediaSerializer
	pagination_class = None
	def get_queryset(self):
		# queryset = TokpedProducts.objects.all()
		# productid = self.request.query_params.get('productid', None)
		# """
		# This view should return a list of all the purchases for
		# the user as determined by the username portion of the URL.
		# """
		# if productid is not None:
		# 	queryset = TokpedProducts.objects.filter(product_id=productid)
		# return queryset
		scheduleid = self.kwargs['scheduleid']
		queryset = TokpedProducts.objects.filter(schedule_id=scheduleid)

		return queryset

class TokopediaFilterProductID(generics.ListAPIView):
	serializer_class = TokopediaSerializer
	def get_queryset(self):
		# queryset = TokpedProducts.objects.all()
		# productid = self.request.query_params.get('productid', None)
		# """
		# This view should return a list of all the purchases for
		# the user as determined by the username portion of the URL.
		# """
		# if productid is not None:
		# 	queryset = TokpedProducts.objects.filter(product_id=productid)
		# return queryset
		scheduleid = self.kwargs['productid']
		queryset = TokpedProducts.objects.filter(product_id=scheduleid)

		return queryset





class TokopediaSearch(generics.ListAPIView):
	queryset = TokpedProducts.objects.all()
	serializer_class = TokopediaSerializer
	filter_backends = [django_filters.rest_framework.DjangoFilterBackend]



class TokopediaExcel(viewsets.ModelViewSet):
	queryset =  TokpedProducts.objects.all()
	parser_classes = (CSVParser,) + tuple(api_settings.DEFAULT_PARSER_CLASSES)
	renderer_classes = (r.CSVRenderer,) + tuple(api_settings.DEFAULT_RENDERER_CLASSES)
	serializer_class = TokopediaCSVSerializer

	def get_renderer_context(self):
		context = super(TokopediaExcel, self).get_renderer_context()
		context['header'] = (
			self.request.GET['fields'].split('====')
			if 'fields' in self.request.GET else None
			)
		return context

	# @list_route(methods=['POST'])
	# def bulk_upload(self, request, *args, **kwargs):
	# 	"""
	# 	Try out this view with the following curl command:
	# 	curl -X POST http://localhost:8000/talks/bulk_upload/ \
	# 		-d "speaker,topic,scheduled_at
	# 			Ana Balica,Testing,2016-11-03T15:15:00+01:00
	# 			Aymeric Augustin,Debugging,2016-11-03T16:15:00+01:00" \
	# 		-H "Content-type: text/csv" \
	# 		-H "Accept: text/csv"
	# 	"""
	# 	serializer = self.get_serializer(data=request.data, many=True)
	# 	serializer.is_valid(raise_exception=True)
	# 	serializer.save()
	# 	return Response(serializer.data, status=status.HTTP_303_SEE_OTHER, headers={'Location': reverse('talk-list')})

# class MyView (APIView):
#     renderer_classes = [CSVRenderer]

#     def get_renderer_context(self):
#         context = super().get_renderer_context()
#         context['header'] = (
#             self.request.GET['fields'].split(',')
#             if 'fields' in self.request.GET else None)
#         return context

# class MyView (APIView):
#     renderer_classes = (r.CSVRenderer, ) + tuple(api_settings.DEFAULT_RENDERER_CLASSES)

# class MyUserRenderer(CSVRenderer):
#     header = ['first', 'last', 'email']

# @api_view(['GET'])
# @renderer_classes((MyUserRenderer,))
# def my_view(request,format='csv'):
# 	users = TokpedProducts.objects.filter(schedule_id=1)
# 	content = [{'first': users.store_id,
# 	            'last': users.product_id,
# 	            'email': users.name}
# 	           for users in users]
# 	return Response(content)



class TokpedCSVProduct(PandasView):
	queryset = TokpedProducts.objects.all()
	serializer_class = TokopediaCSVSerializer
	filename = 'download.csv'
	def get_queryset(self,format='csv'):
		# """
		# This view should return a list of all the purchases for
		# the user as determined by the username portion of the URL.
		# """
		productid = self.kwargs['productid']
		queryset = TokpedProducts.objects.filter(product_id=productid)

		return queryset
	def get_pandas_filename(self, request, format):
		if format in ('xls', 'xlsx','csv'):
			# Use custom filename and Content-Disposition header
			return "Data Export"  # Extension will be appended automatically
		else:
			# Default filename from URL (no Content-Disposition header)
			return None


class TokpedCSVSchedule(PandasView):
	queryset = TokpedProducts.objects.all()
	serializer_class = TokopediaCSVSerializer
	filename = 'download.csv'
	def get_queryset(self,format='csv'):
		# """
		# This view should return a list of all the purchases for
		# the user as determined by the username portion of the URL.
		# """
		scheduleid = self.kwargs['scheduleid']
		queryset = TokpedProducts.objects.filter(schedule_id=scheduleid)

		return queryset
	def get_pandas_filename(self, request, format):
		if format in ('xls', 'xlsx','csv'):
			# Use custom filename and Content-Disposition header
			# return "Data Export"  # Extension will be appended automatically
		else:
			# Default filename from URL (no Content-Disposition header)
			return None