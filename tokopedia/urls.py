from django.urls import path
from .views import TokopediaList,TokopediaDetail,TokopediaFilterScheduleID,TokopediaSearch,TokopediaExcel,TokpedCSV,TokopediaFilterProductID,my_view
from rest_framework.urlpatterns import format_suffix_patterns
urlpatterns = [
    path('',TokopediaList.as_view()),
    path('<int:pk>/',TokopediaDetail.as_view()),
    path('filter/scheduleid/<int:scheduleid>/',TokopediaFilterScheduleID.as_view()),
    path('filter/product-name/',TokopediaSearch.as_view()),
    path('export/',TokopediaExcel.as_view({'get': 'list'})),
    path('print/csv/<int:productid>/',TokpedCSV.as_view()),
    path('filter/productid/<int:productid>/',TokopediaFilterProductID.as_view()),


]
urlpatterns = format_suffix_patterns(urlpatterns,allowed=['json', 'html','csv'])