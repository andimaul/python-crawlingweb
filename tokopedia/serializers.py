from rest_framework import serializers
from .models import TokpedProducts

class TokopediaSerializer(serializers.ModelSerializer):
	class Meta:
		model = TokpedProducts
		fields = '__all__'

class TokopediaCSVSerializer(serializers.ModelSerializer):
	class Meta:
		model = TokpedProducts
		fields = ['store_id','product_id','name']

		# ['store_id','product_id','name','price_before_discount','price','url','rate','sold','transaction_success']