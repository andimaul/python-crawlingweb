from django.db import models

# Create your models here.
class Tasks(models.Model):
    id = models.BigAutoField(primary_key=True)
    user_id = models.CharField(max_length=191)
    task_type_id = models.CharField(max_length=191)
    category_id = models.CharField(max_length=191, blank=True, null=True)
    scheduling_type_id = models.CharField(max_length=191)
    name = models.CharField(max_length=191)
    crawl_start = models.DateField()
    crawl_end = models.DateField()
    email = models.EmailField(max_length=254, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tasks'

class Keywords(models.Model):
    id = models.BigAutoField(primary_key=True)
    task_id = models.CharField(max_length=191)
    platform = models.CharField(max_length=191)
    keyword = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'keywords'