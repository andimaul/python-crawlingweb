from django.db import models

# Create your models here.
class ShopeeProducts(models.Model):
    id = models.BigAutoField(primary_key=True)
    schedule_id = models.CharField(max_length=191)
    store_id = models.CharField(max_length=191)
    product_id = models.CharField(max_length=191)
    url_id = models.CharField(max_length=191)
    keyword_id = models.CharField(max_length=191, blank=True, null=True)
    product_name = models.CharField(max_length=191)
    price = models.CharField(max_length=191)
    price_before_discount = models.CharField(max_length=191)
    url = models.CharField(max_length=191)
    image = models.CharField(max_length=191)
    rate = models.IntegerField()
    sold = models.IntegerField()
    transaction_success = models.CharField(max_length=191)
    total_review = models.IntegerField()
    view = models.IntegerField()
    category = models.CharField(max_length=191)
    sub_category = models.CharField(max_length=191)
    subdetail = models.CharField(max_length=191)
    status_produk = models.CharField(max_length=191)
    like = models.CharField(max_length=191)
    upload_date = models.DateField(db_column='upload date')  # Field renamed to remove unsuitable characters.
    sales = models.CharField(max_length=191, blank=True, null=True)
    conversion_rate = models.CharField(max_length=191, blank=True, null=True)
    view_chart = models.CharField(max_length=191, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'shopee_products'