from django.shortcuts import render
from .serializers import ShopeeSerializer
from .models import ShopeeProducts
from rest_framework import generics,authentication, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
import django_filters.rest_framework
from django.urls import reverse
from rest_framework import viewsets, status
# Create your views here.


class ShopeeList(generics.ListAPIView):
	queryset= ShopeeProducts.objects.all()
	serializer_class = ShopeeSerializer

class ShopeeFilterScheduleID(generics.ListAPIView):
	serializer_class = ShopeeSerializer
	pagination_class = None
	def get_queryset(self):
		scheduleid = self.kwargs['scheduleid']
		queryset = ShopeeProducts.objects.filter(schedule_id=scheduleid)
		return queryset

class ShopeeFilterProductID(generics.ListAPIView):
	serializer_class = ShopeeSerializer
	def get_queryset(self):
		productid = self.kwargs['productid']
		queryset = ShopeeProducts.objects.filter(product_id=productid)
		return queryset

