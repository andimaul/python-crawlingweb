from rest_framework import serializers
from .models import ShopeeProducts

class ShopeeSerializer(serializers.ModelSerializer):
	class Meta:
		model = ShopeeProducts
		fields = '__all__'
