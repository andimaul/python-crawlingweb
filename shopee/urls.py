from django.urls import path 
from .views import ShopeeList,ShopeeFilterScheduleID,ShopeeFilterProductID

urlpatterns = [
		path('',ShopeeList.as_view()),
		path('filter/scheduleid/<int:scheduleid>/',ShopeeFilterScheduleID.as_view()),
		path('filter/productid/<int:productid>/',ShopeeFilterProductID.as_view()),
]
