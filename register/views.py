from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from .serializers import UserSerializer
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
# Create your views here.

class Register(generics.CreateAPIView):


	# def create(self,request,validated_data):
	# 	username= request.POST.get(validated_data['username'])
	# 	email = request.POST.get( validated_data['email'])
	# 	password = request.POST.get(validated_data['password'])  
	# 	user = User.objects.create_user('username','email','password')
	# 	return user

	def post(self,request,*args,**kwargs):
		#Creating New User
		username= request.data.get('username')
		email = request.data.get('email')
		password = request.data.get('password')  
		user = User.objects.create_user(username,email,password)
		user.save()

		#Generate token
		token = Token.objects.create(user=user)
		return Response({'detail': 'User has been created with Token:' + token.key })