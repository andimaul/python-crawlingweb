# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class BukalapakProducts(models.Model):
    id = models.BigAutoField(primary_key=True)
    schedule_id = models.BigIntegerField()
    url_id = models.BigIntegerField()
    keyword_id = models.CharField(max_length=191, blank=True, null=True)
    store_name = models.CharField(max_length=191)
    url_product = models.CharField(max_length=300)
    product_name = models.CharField(max_length=300)
    review = models.CharField(max_length=191)
    original_price = models.CharField(max_length=191)
    price = models.CharField(max_length=191)
    stock = models.CharField(max_length=191)
    sold = models.CharField(max_length=191)
    view = models.CharField(max_length=191)
    favorite = models.CharField(max_length=191)
    rate = models.CharField(max_length=191)
    category = models.CharField(max_length=191)
    sub_category = models.CharField(max_length=191)
    sub_detail_category = models.CharField(max_length=191)
    url_image = models.CharField(max_length=300)
    location = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'bukalapak_products'


class BukalapakStores(models.Model):
    id = models.BigAutoField(primary_key=True)
    schedule_id = models.BigIntegerField()
    url_id = models.BigIntegerField()
    store_name = models.CharField(max_length=191)
    url_store = models.CharField(max_length=191)
    location = models.CharField(max_length=191)
    review = models.CharField(max_length=191)
    customer = models.CharField(max_length=191)
    feedback = models.CharField(max_length=191)
    estimated_delivery = models.CharField(max_length=191)
    join_date = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'bukalapak_stores'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Keywords(models.Model):
    id = models.BigAutoField(primary_key=True)
    task_id = models.CharField(max_length=191)
    platform = models.CharField(max_length=191)
    keyword = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'keywords'


class LazadaProducts(models.Model):
    id = models.BigAutoField(primary_key=True)
    schedule_id = models.BigIntegerField()
    product_id = models.BigIntegerField()
    url_id = models.BigIntegerField()
    keyword_id = models.CharField(max_length=191, blank=True, null=True)
    url_product = models.CharField(max_length=300)
    store_name = models.CharField(max_length=300)
    product_name = models.CharField(max_length=300)
    price = models.CharField(max_length=191)
    price_before_discount = models.CharField(max_length=191)
    discount = models.CharField(max_length=191)
    url_image = models.CharField(max_length=300)
    rate = models.CharField(max_length=191)
    review = models.CharField(max_length=191)
    location = models.CharField(max_length=191)
    shipping_from_aboard = models.CharField(max_length=191)
    category = models.CharField(max_length=191)
    sub_category = models.CharField(max_length=191)
    sub_detail_category = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lazada_products'


class LazadaStores(models.Model):
    id = models.BigAutoField(primary_key=True)
    schedule_id = models.BigIntegerField()
    url_id = models.BigIntegerField()
    store_name = models.CharField(max_length=191)
    url_store = models.CharField(max_length=191)
    followers = models.CharField(max_length=191)
    rate_positive_seller = models.CharField(max_length=191)
    prime_category = models.CharField(max_length=191)
    location = models.CharField(max_length=191)
    join_date = models.CharField(max_length=191)
    response = models.CharField(max_length=191)
    review_seller = models.CharField(max_length=191)
    review_positif = models.CharField(max_length=191)
    review_negatif = models.CharField(max_length=191)
    review_netral = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lazada_stores'


class Marketplace(models.Model):
    id = models.BigAutoField(primary_key=True)
    marketplace = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'marketplace'


class Migrations(models.Model):
    migration = models.CharField(max_length=191)
    batch = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'migrations'


class PasswordResets(models.Model):
    email = models.CharField(max_length=191)
    token = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'password_resets'


class PollsChoice(models.Model):
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField()
    question = models.ForeignKey('PollsQuestion', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'polls_choice'


class PollsQuestion(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'polls_question'


class ScheduleTypes(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'schedule_types'


class Schedules(models.Model):
    id = models.BigAutoField(primary_key=True)
    task_id = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'schedules'


class ShopeeProducts(models.Model):
    id = models.BigAutoField(primary_key=True)
    schedule_id = models.CharField(max_length=191)
    store_id = models.CharField(max_length=191)
    product_id = models.CharField(max_length=191)
    url_id = models.CharField(max_length=191)
    keyword_id = models.CharField(max_length=191, blank=True, null=True)
    product_name = models.CharField(max_length=191)
    price = models.CharField(max_length=191)
    price_before_discount = models.CharField(max_length=191)
    url = models.CharField(max_length=191)
    image = models.CharField(max_length=191)
    rate = models.IntegerField()
    sold = models.IntegerField()
    transaction_success = models.CharField(max_length=191)
    total_review = models.IntegerField()
    view = models.IntegerField()
    category = models.CharField(max_length=191)
    sub_category = models.CharField(max_length=191)
    subdetail = models.CharField(max_length=191)
    status_produk = models.CharField(max_length=191)
    like = models.CharField(max_length=191)
    upload_date = models.DateField(db_column='upload date')  # Field renamed to remove unsuitable characters.
    sales = models.CharField(max_length=191, blank=True, null=True)
    conversion_rate = models.CharField(max_length=191, blank=True, null=True)
    view_chart = models.CharField(max_length=191, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'shopee_products'


class ShopeeStores(models.Model):
    id = models.BigAutoField(primary_key=True)
    schedule_id = models.CharField(max_length=191)
    store_name = models.CharField(max_length=191)
    store_id = models.IntegerField()
    url_id = models.CharField(max_length=191)
    url = models.CharField(max_length=191)
    location = models.CharField(max_length=191)
    product = models.CharField(max_length=191)
    wrapping_time = models.CharField(max_length=191)
    following = models.CharField(max_length=191)
    chat_performe = models.CharField(max_length=191)
    estimated_time_of_chat = models.CharField(max_length=191)
    followers = models.CharField(max_length=191)
    rate = models.CharField(max_length=191)
    poin_rating = models.CharField(max_length=191)
    join_date = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'shopee_stores'


class Tasks(models.Model):
    id = models.BigAutoField(primary_key=True)
    user_id = models.CharField(max_length=191)
    task_type_id = models.CharField(max_length=191)
    category_id = models.CharField(max_length=191, blank=True, null=True)
    scheduling_type_id = models.CharField(max_length=191)
    name = models.CharField(max_length=191)
    crawl_start = models.DateField()
    crawl_end = models.DateField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tasks'


class TokpedProducts(models.Model):
    id = models.BigAutoField(primary_key=True)
    schedule_id = models.CharField(max_length=191)
    store_id = models.CharField(max_length=191)
    product_id = models.CharField(max_length=191)
    url_id = models.CharField(max_length=191, blank=True, null=True)
    keyword_id = models.CharField(max_length=191, blank=True, null=True)
    name = models.CharField(max_length=191)
    price_before_discount = models.CharField(max_length=191)
    discount = models.CharField(max_length=191)
    price = models.FloatField()
    url = models.CharField(max_length=191)
    image = models.CharField(max_length=191)
    rate = models.IntegerField()
    sold = models.IntegerField()
    transaction_success = models.CharField(max_length=191)
    total_review = models.IntegerField()
    view = models.IntegerField()
    location = models.CharField(max_length=191)
    category = models.CharField(max_length=191)
    sub_category = models.CharField(max_length=191)
    subdetail = models.CharField(max_length=191)
    status_produk = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    sales = models.CharField(max_length=191, blank=True, null=True)
    conversion_rate = models.CharField(max_length=191, blank=True, null=True)
    view_chart = models.CharField(max_length=191, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tokped_products'


class TokpedStores(models.Model):
    id = models.BigAutoField(primary_key=True)
    schedule_id = models.CharField(max_length=191)
    name = models.CharField(max_length=191)
    url_id = models.CharField(max_length=191)
    url = models.CharField(max_length=191)
    followers = models.CharField(max_length=191)
    review = models.CharField(max_length=191)
    etalase = models.CharField(max_length=191)
    product_active = models.CharField(max_length=191)
    transaction = models.CharField(max_length=191)
    location = models.CharField(max_length=191)
    rating = models.CharField(max_length=191)
    response = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tokped_stores'


class Urls(models.Model):
    id = models.BigAutoField(primary_key=True)
    task_id = models.CharField(max_length=191)
    platform = models.CharField(max_length=191)
    link = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'urls'


class Users(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=191)
    email = models.CharField(unique=True, max_length=191)
    email_verified_at = models.DateTimeField(blank=True, null=True)
    password = models.CharField(max_length=191)
    remember_token = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'
