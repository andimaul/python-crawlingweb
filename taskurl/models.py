from django.db import models

# Create your models here.
class Tasks(models.Model):
    id = models.BigAutoField(primary_key=True)
    user_id = models.CharField(max_length=191)
    task_type_id = models.CharField(max_length=191)
    category_id = models.CharField(max_length=191, blank=True, null=True)
    scheduling_type_id = models.CharField(max_length=191)
    name = models.CharField(max_length=191)
    crawl_start = models.DateField()
    crawl_end = models.DateField()
    email = models.EmailField(max_length=254, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tasks'

class Urls(models.Model):
    id = models.BigAutoField(primary_key=True)
    task_id = models.CharField(max_length=191)
    platform = models.CharField(max_length=191)
    link = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'urls'


# class AuthUser(models.Model):
#     password = models.CharField(max_length=128)
#     last_login = models.DateTimeField(blank=True, null=True)
#     is_superuser = models.IntegerField()
#     username = models.CharField(unique=True, max_length=150)
#     first_name = models.CharField(max_length=30)
#     last_name = models.CharField(max_length=150)
#     email = models.CharField(max_length=254)
#     is_staff = models.IntegerField()
#     is_active = models.IntegerField()
#     date_joined = models.DateTimeField()

#     class Meta:
#         managed = False
#         db_table = 'auth_user'

# class AuthtokenToken(models.Model):
#     key = models.CharField(primary_key=True, max_length=40)
#     created = models.DateTimeField()
#     user = models.ForeignKey(AuthUser, models.DO_NOTHING, unique=True)

#     class Meta:
#         managed = False
#         db_table = 'authtoken_token'

