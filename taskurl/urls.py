from django.urls import path
from .views import Task,TaskRunning,TaskDetail,TaskUpdate
urlpatterns = [
	path('',Task.as_view()),
	path('running/', TaskRunning.as_view()),
	path('running/detail/<int:pk>', TaskDetail.as_view()),
	path('update/<int:pk>/',TaskUpdate.as_view())
	]