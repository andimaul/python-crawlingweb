from rest_framework import serializers
from .models import Tasks


class TasksSerializer(serializers.ModelSerializer):
	class Meta:
		model = Tasks
		fields = ['task_type_id','category_id','scheduling_type_id','name','crawl_start','crawl_end','email']
