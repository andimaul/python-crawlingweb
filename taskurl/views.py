from django.shortcuts import render
from rest_framework import generics
from .models import Tasks,Urls
from django.utils import timezone
from rest_framework.response import Response
from .serializers import TasksSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.authentication import SessionAuthentication, BasicAuthentication,TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import filters
from smtplib import SMTP
import smtplib 
import mysql.connector
# Create your views here.

class Task(generics.CreateAPIView):
	queryset = Tasks.objects.all()
	serializer_class = TasksSerializer
	# authentication_classes = [TokenAuthentication]
	permission_classes = [IsAuthenticated]
	
		

	def post(self,request,*args,**kwargs):
		try:
			connection = mysql.connector.connect\
			(host = "localhost", user = "root", passwd ="", db = "db_digitalkita")
			# connection = mysql.connector.connect\
			# (host = "localhost", user = "crawler", passwd ="crawler_g1lA", db = "digitalkita")
			print("DB SUCCESS")
		except:
			print("No connection")
			sys.exit(0)
		cursor = connection.cursor()
		user_id = request.user.id
		print("ini token "+str(user_id))
		task_type_id = request.data.get('task_type_id')
		category_id = request.data.get('category_id')
		scheduling_type_id = request.data.get('scheduling_type_id')
		name = request.data.get('name')
		crawl_start = request.data.get('crawl_start')
		crawl_end = request.data.get('crawl_end')
		created_at = timezone.now()
		update_at = timezone.now()
		email = request.data.get('email')

		task = Tasks.objects.create(user_id=user_id,task_type_id=task_type_id,category_id=category_id,scheduling_type_id=scheduling_type_id ,name=name,crawl_start=crawl_start,crawl_end=crawl_end,created_at=created_at,updated_at=update_at,email=email)
		task.save()
		task_id = task.id
		platform =  request.data.get('platform')
		link = request.data.get('link')
		created_at = timezone.now()
		updated_at = timezone.now()
		url = Urls.objects.create(task_id=task_id,platform=platform,link=link,created_at=created_at,updated_at=updated_at)
		url.save()
		cursor.execute("SELECT name FROM tasks ORDER BY id DESC LIMIT 1")
		result = cursor.fetchone()
		result = result[0]
		cursor.execute("SELECT platform,link FROM urls ORDER BY id DESC LIMIT 1")
		result_urls = cursor.fetchone()
		marketplace = result_urls[0]
		url = result_urls[1]
		print(result)

		print(result_urls)

		sender = 'it.gabislipi@gmail.com'
		receivers = ['andimaull@gmail.com']

		message = "Subject:{}\n\nTask Name:{}\n\nMarketplace:{}\n\nURL:{}".format("Crawling",result,marketplace,url)

		smtpObj = smtplib.SMTP(host='smtp.gmail.com', port=587)
		smtpObj.ehlo()
		smtpObj.starttls()
		smtpObj.ehlo()
		smtpObj.login('it.gabislipi@gmail.com','kerjadig4b1')
		smtpObj.sendmail(sender, receivers, message)
		smtpObj.quit()
		print ("Successfully sent email")
		# logger.addHandler(smtp_handler)
		print("send email")		


		return Response('ok')

class TaskRunning(generics.ListAPIView):
	queryset = Tasks.objects.all()
	serializer_class = TasksSerializer
	permission_classes = [IsAuthenticated]
	filter_backends = [filters.OrderingFilter]
	ordering_fields = ['crawl_start']

class TaskDetail(generics.RetrieveAPIView):
	serializer_class = TasksSerializer
	queryset = Tasks.objects.all()


class TaskUpdate(generics.RetrieveUpdateAPIView):
	queryset = Tasks.objects.all()
	serializer_class = TasksSerializer

