from django.urls import path
from rest_framework.authtoken import views as vw
urlpatterns = [
	path('',vw.obtain_auth_token),
]