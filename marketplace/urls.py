from django.urls import path 
from .views import MarketplaceList,MarketplaceDetail

urlpatterns = [
	path('',MarketplaceList.as_view()),
	path('<int:pk>/',MarketplaceDetail.as_view()),
]