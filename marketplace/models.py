from django.db import models

# Create your models here.
class Marketplace(models.Model):
    id = models.BigAutoField(primary_key=True)
    marketplace = models.CharField(max_length=191)
    created_at = models.DateTimeField(auto_now=True,blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        managed = True
        db_table = 'marketplace'