from django.shortcuts import render
from rest_framework import generics
from .models import Marketplace
from .serializers import MarketplaceSerializer
# Create your views here.

class MarketplaceList(generics.ListCreateAPIView):
	queryset = Marketplace.objects.all()
	serializer_class = MarketplaceSerializer

class MarketplaceDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = Marketplace.objects.all()
	serializer_class = MarketplaceSerializer